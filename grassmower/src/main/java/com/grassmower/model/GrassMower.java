package com.grassmower.model;

public class GrassMower {

    private Integer x;
    private int y;
    private char dir;

    private String commands;

    public GrassMower() {
		super();
	}


	public GrassMower(int x, int y, char dir, String commands) {
        this.x = x;
        this.y = y;
        this.dir = dir;
        this.commands = commands;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public char getDir() {
        return dir;
    }

    public void setDir(char dir) {
        this.dir = dir;
    }

    public String getCommands() {
        return commands;
    }

    public void setCommands(String commands) {
        this.commands = commands;
    }

    @Override
    public String toString() {
        return x + " " + y + " " + dir ;
    }
}
