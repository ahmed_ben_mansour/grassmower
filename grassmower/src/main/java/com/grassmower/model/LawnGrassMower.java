package com.grassmower.model;

import java.util.ArrayList;
import java.util.List;

public class LawnGrassMower {
    private Lawn lawn;
    private List<GrassMower> grassMowers;

    public LawnGrassMower(Lawn lawn, List<GrassMower> grassMowers) {
        this.lawn = lawn;
        this.grassMowers = grassMowers;
    }

    public LawnGrassMower() {
        this.lawn = new Lawn();
        this.grassMowers = new ArrayList<GrassMower>();
    }

    public Lawn getLawn() {
        return lawn;
    }

    public void setLawn(Lawn lawn) {
        this.lawn = lawn;
    }

    public List<GrassMower> getGrassMowers() {
        return grassMowers;
    }

    public void setGrassMowers(List<GrassMower> grassMowers) {
        this.grassMowers = grassMowers;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (GrassMower gm : getGrassMowers()) {
            str.append(gm.toString());
        }
        return str.toString();
    }
}
