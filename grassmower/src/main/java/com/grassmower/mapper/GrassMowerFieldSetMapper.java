package com.grassmower.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.grassmower.model.GrassMower;

public class GrassMowerFieldSetMapper  implements FieldSetMapper<GrassMower> {

	@Override
	public GrassMower mapFieldSet(FieldSet fieldSet) throws BindException {
		GrassMower grassMower  = new GrassMower();
		
		grassMower.setX(fieldSet.readInt(0));
		grassMower.setY(fieldSet.readInt(1));
		grassMower.setDir(fieldSet.readChar(2));
		grassMower.setCommands(fieldSet.readString(3));
		return grassMower;
	}

}
