package com.grassmower.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.grassmower.model.Lawn;

public class LawnFieldSetMapper implements FieldSetMapper<Lawn>{

	@Override
	public Lawn mapFieldSet(FieldSet fieldSet) throws BindException {
		Lawn  lawn = new Lawn();
		
		lawn.setMAX_X(fieldSet.readInt(0));
		lawn.setMAX_Y(fieldSet.readInt(1));
		return lawn;
	}

}
