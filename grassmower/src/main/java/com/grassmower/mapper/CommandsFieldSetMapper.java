package com.grassmower.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class CommandsFieldSetMapper implements FieldSetMapper<String>{

	@Override
	public String mapFieldSet(FieldSet fieldSet) throws BindException {
		return fieldSet.readString(0);
	}

}
