package com.grassmower.batch;

import java.util.HashMap;

import org.springframework.batch.item.ItemProcessor;

import org.javatuples.Pair;

import com.grassmower.datastructure.DoublyCircularLinkedList;
import com.grassmower.datastructure.interfaces.IDoublyCircularLinkedList;
import com.grassmower.model.GrassMower;




public class BatchProcessor implements ItemProcessor<GrassMower, GrassMower> {

    HashMap<Character, Pair<Integer, Integer>> directionEffect;
    IDoublyCircularLinkedList directions;
    public BatchProcessor() {
        directionEffect = new HashMap<>();
        directionEffect.put('N', new Pair<>(0, 1));
        directionEffect.put('E', new Pair<>(1, 0));
        directionEffect.put('W', new Pair<>(-1, 0));
        directionEffect.put('S', new Pair<>(0, -1));

        directions = new DoublyCircularLinkedList(); // = {'N', 'E', 'S', 'W'};

        // Clockwise direction
        directions.addNode('N');
        directions.addNode('E');
        directions.addNode('S');
        directions.addNode('W');
    }

    @Override
    public GrassMower process(GrassMower grassMower) throws Exception {

        // Set a pointer to the directions list
        directions.setCurrent(grassMower.getDir());

        String commands = grassMower.getCommands();
        commands.chars().forEach(c -> {
            switch ((char) c) {
                case 'D': // next()
                    directions.next();
                    grassMower.setDir(directions.getCurrentDir());
                    break;
                case 'G': // previous()
                    directions.previous();
                    grassMower.setDir(directions.getCurrentDir());
                    break;
                case 'A':
                    int updatedX = grassMower.getX() + (directionEffect.get(directions.getCurrentDir())).getValue0();
                    int updatedY= grassMower.getY() + (directionEffect.get(directions.getCurrentDir())).getValue1();

                    grassMower.setX(updatedX);
                    grassMower.setY(updatedY);
                    break;
                default:
                    break;
            }
        });
        return grassMower;
    }
}
