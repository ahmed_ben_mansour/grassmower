package com.grassmower.batch;


import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.support.DefaultBatchConfiguration;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.PassThroughLineAggregator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.PlatformTransactionManager;

import com.grassmower.model.GrassMower;

@Configuration
@ConditionalOnMissingBean(value = DefaultBatchConfiguration.class, annotation = EnableBatchProcessing.class)
public class BatchConfig {


    private final JobRepository jobRepository;
    private final PlatformTransactionManager platformTransactionManager;
    
    public BatchConfig(final JobRepository jobRepository, final PlatformTransactionManager platformTransactionManager) {
    	this.jobRepository = jobRepository;
    	this.platformTransactionManager = platformTransactionManager;
    }

    @Bean
    public FlatFileItemReader<GrassMower> reader() {
        FlatFileItemReader<GrassMower> itemReader = new FlatFileItemReader<>();
        itemReader.setResource(new FileSystemResource("src/main/resources/values.txt"));
        itemReader.setName("txtReader");
        itemReader.setLinesToSkip(1); // Skip the lawn-dimensions line
        itemReader.setStrict(true);
        itemReader.setLineMapper(lineMapper());
        return itemReader;
    }


    private LineMapper<GrassMower> lineMapper() {
        DefaultLineMapper<GrassMower> lineMapper = new DefaultLineMapper<>();

        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter(" ");
        lineTokenizer.setNames("x", "y", "dir", "commands");

        BeanWrapperFieldSetMapper<GrassMower> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(GrassMower.class);

        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(fieldSetMapper);

        return lineMapper;
    }
    @Bean
    public BatchProcessor processor() {
        return new BatchProcessor();
    }

    @Bean
    public FlatFileItemWriter<GrassMower> writer() {
        FlatFileItemWriter<GrassMower> writer = new FlatFileItemWriter<>();
        writer.setName("txtWriter");
        writer.setResource(new FileSystemResource("src/main/resources/results.txt"));
        writer.setLineAggregator(new PassThroughLineAggregator<>());
        return writer;
    }


    @Bean
    public Step mowerStep() throws Exception{
        return new StepBuilder("mowerStep", jobRepository)
                .<GrassMower, GrassMower>chunk(10, platformTransactionManager)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                // .taskExecutor(taskExecutor()) // For concurrency
                .build();
    }

    @Bean
    public Job runJob() throws Exception {
        return new JobBuilder("mowerJob", jobRepository)
                .start(mowerStep())
                .build();
    }

}
