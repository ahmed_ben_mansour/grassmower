package com.grassmower;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrassMowerApplication implements CommandLineRunner {

	private final JobLauncher jobLauncher;
	private final JobLocator jobLocator;

	public GrassMowerApplication(JobLauncher jobLauncher, JobLocator jobLocator) {
		this.jobLauncher = jobLauncher;
		this.jobLocator = jobLocator;
	}

	public static void main(String[] args) {
		SpringApplication.run(GrassMowerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String jobName = args.length > 0 ? args[0] : "mowerJob"; // Handle job name from arguments
		Job job = jobLocator.getJob(jobName);

		JobParameters jobParameters = new JobParametersBuilder()
				.addLong("startAt", System.currentTimeMillis())
				.toJobParameters();

		try {
			jobLauncher.run(job, jobParameters);
		} catch (JobExecutionAlreadyRunningException
				| JobRestartException
				| JobInstanceAlreadyCompleteException
				| JobParametersInvalidException e) {
			e.printStackTrace();
		}

	}
}
