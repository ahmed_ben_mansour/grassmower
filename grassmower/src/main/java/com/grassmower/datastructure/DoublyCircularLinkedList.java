package com.grassmower.datastructure;

import com.grassmower.datastructure.interfaces.IDoublyCircularLinkedList;

public class DoublyCircularLinkedList implements IDoublyCircularLinkedList{
    private Node head;
    private Node tail;
    private Node current; // Current node for iteration

    public DoublyCircularLinkedList() {
        this.head = null;
        this.tail = null;
        this.current = null;

    }

    public void addNode(char value) {
        Node newNode = new Node(value);
        if (head == null) {
            head = newNode;
            tail = newNode;
            current = newNode;
        } else {
            tail.next = newNode;
            newNode.prev = tail;
            tail = newNode;
            tail.next = head; // Make it circular
            head.prev = tail; // Ensure head's prev points to tail
        }
    }

    public Node get(char value) {
        if (head == null) {
            return null;
        }

        Node current = head;
        do {
            if (current.data == value) {
                return current;
            }
            current = current.next;
        } while (current != head);

        return null;
    }

    public Node next() {
        if (current != null) {
            current = current.next;
            return current;
        }
        return null;
    }

    public Node previous() {
        if (current != null) {
            current = current.prev;
            return current;
        }
        return null;
    }

    public void setCurrent(char value) {
        Node node = get(value);
        if (node != null) {
            current = node;
        }
    }

    public char getCurrentDir() {
        if (current != null) {
            return current.data;
        }
        return '\0'; // Return null character if current is null
    }

}
