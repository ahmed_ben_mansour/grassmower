package com.grassmower.datastructure.interfaces;

import com.grassmower.datastructure.Node;

public interface IDoublyCircularLinkedList {
	 public void addNode(char value);
	    public Node get(char value);

	    public Node next();
	    public Node previous();

	    public void setCurrent(char value);
	    public char getCurrentDir();
}
