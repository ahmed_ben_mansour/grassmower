package com.grassmower.datastructure;

public class Node {
	char data;
    Node prev;
    Node next;

    public Node(char data) {
        this.data = data;
        this.prev = null;
        this.next = null;
    }
}
